
public class ArrayUtils{


	public static int findBinarySearch(int a[], int data){
		int low = 0, high = a.length - 1;

		while(low <= high){
			int mid = low + (high - low) / 2;

			if(data < a[mid])
				high = mid - 1;
			else if (data > a[mid])
				low = mid + 1;
			else 
				return mid;
		}

		return -1;
	}


	public static int findPivotIndex(int a[]){
		int low = 0, high = a.length - 1;

		if(a[low] <= a[high])
			return low;

		while(low <= high){
			int mid = low + (high - low) / 2;

			int next = (mid + 1 + a.length) % a.length;
			int prev = (mid - 1 + a.length) & a.length;

			if(a[prev] > a[mid] && a[mid] < a[next])
				return mid;

			else if (a[mid] > a[high])
				low = mid + 1;
			else 
				high = mid - 1;
		}

		return -1;
	}


	public static int findFirstOccuranceOfNumber(int a[], int data){
		int low = 0, high = a.length - 1;
		int result = -1;
		while(low <= high){
			int mid = low + (high - low) / 2;

			if(data < a[mid])
				high = mid - 1;
			else if (data > a[mid])
				low = mid + 1;
			else {
				result = mid;
				high = mid - 1;
			}
		}
		return result;
	}

	public static int findLastOccuranceOfNumber(int a[], int data){
		int low = 0, high = a.length - 1;
		int result = -1;

		while(low <= high){
			int mid = low + (high - low) / 2;
			if(data < a[mid])
				high = mid - 1;
			else if(data > a[mid]){
				low = mid + 1;
			} else {
				result = mid;
				low = mid + 1;
			}
		}

		return result;
	}

	public static int findCount(int a[], int number){
		int startIndex = findFirstOccuranceOfNumber(a, number);
		int endIndex = findLastOccuranceOfNumber(a, number);

		if(startIndex != -1 || endIndex != -1)
			return endIndex - startIndex + 1;
		else return 0;
	}

	public static int binarySearch(int a[], int data){
		int low = 0, high = a.length - 1;

		while(low <= high){
			int mid = low + (high - low) / 2;

			if(a[mid] > data)
				high = mid - 1;
			else if (a[mid] <  data)
				low = mid + 1;
			else 
				return mid;
		}
		return -1;
	}

	public static int binarySearchRec(int a[], int low, int high, int value){
		if(low > high)
			return -1;
		int mid = low + (high - low) / 2;
		if(a[mid] == value)
			return mid;
		else if(a[mid] > value)
			return binarySearchRec(a, low, mid - 1 , value);
		else 
			return binarySearchRec(a, mid + 1, high, value);
	}

	public static int binarySearchPivoted(int a[], int data){
		int low = 0, high = a.length - 1;

		while(low <= high){

			int mid = low + (high - low) / 2;

			if(a[mid] == data)
				return mid;

			else if(a[low] <= a[mid]){
				// left half does not have pivot
				if(data >= a[low] && data < a[mid])
					high = mid - 1;
				else
					low = mid + 1;
			} else {
				// right half does not have pivot
				if(data > a[mid] && data <= a[high])
					low = mid + 1;
				else {
					high = mid - 1;
				}
			}
		}

		return -1;
	}

	public static int findPivotBinary(int a[]){
		int low = 0, high = a.length -1;

		if(a[low] <= a[high])
			return low;

		while(low <= high){		
			int mid = low + (high - low) /2 ;

			int next = (mid + 1 + a.length) % a.length;
			int prev = (mid - 1 + a.length) % a.length;

			if(a[mid] < a[prev] && a[mid] < a[next])
				return mid;

			if(a[mid] < a[high])
				high = mid - 1;
			else
				low = mid + 1;

		}
		return -1;
	}

	public static void reverseArray(int a[]){
		int low = 0, high = a.length - 1;

		while(low < high){
			a[low] = a[low] ^ a[high];
			a[high] = a[low] ^ a[high];
			a[low] = a[low] ^ a[high];

			low ++;
			high --;
		}
	}

	public static int findPivot(int a[]){

		int low = 0, high = a.length - 1;

		while(a[low] > a[high]){
			int mid = low + (high - low) / 2;

			if(a[mid] > a[high])
				low = mid + 1;
			else
				high = mid;
		}
		return low;
	}

	public static void main(String args[]){
		int a[] = {0,1,1,1,1,1,2,3,4,7};

		//int r[] = {4,5,6,7,8, 9, 0, 1, 2, 3};

		int b[] = {1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1};

		//System.out.println(findPivotBinary(b));
		//System.out.println(findPivot(b));
		//System.out.println(binarySearchPivoted(r, 2));
		//System.out.println(binarySearchRec(a, 0, a.length - 1, 7));
		System.out.println(findCount(a, 7));
		//System.out.println(findLastOccuranceOfNumber(a, 1));
		//System.out.println(findFirstOccuranceOfNumber(a, 1));
	}
}


